#version 120 
#extension GL_EXT_geometry_shader4 : enable
varying out vec3 myNormal;
uniform int segs;
varying out float lightIntensity;  // one per vertex
uniform float radius;
/////////////////////新加上的
uniform int which;
uniform float vet[9];
/////////////////////
vec3 rodriguesRot (vec3 v, vec3 axis, float rad)
{
	return v*cos(rad) + cross(axis,v)*sin(rad) + axis*dot(axis,v)*(1-cos(rad));
}

vec3 getSpan(vec3 inaxis)
{

	vec3 axis = normalize(inaxis);
	
	// find a basis with least projection, then gram-schmidt to get perperdicular axis
	int min;
	float minproj = 10;
	vec3 basis[3];
	basis[0] = vec3(1,0,0); basis[1] = vec3(0,1,0); basis[2] = vec3(0,0,1);
	for (int i = 0; i < 3; i++) {
		float m;
		if ((m=abs(dot (axis, basis[i]))) < minproj) {     //abs:絕對值
			minproj = m;
			min = i;
		}
	}
	
	return normalize(basis[min] - dot(axis,basis[min])*axis);
}

void main(void)
{
	vec3 axis_f, axis_b;
	vec3 spine_f, spine_b;
	vec3 span_f, span_b;
	vec3 turned_span_f,turned_span_b;
	
	int i, j;

#if 0    
	//Pass-thru!
	for(int i=0; i< gl_VerticesIn; i++){
		gl_Position = gl_ModelViewProjectionMatrix * gl_PositionIn[i];
		EmitVertex();
	}
	EndPrimitive();
#endif

	vec3 ecnormal;
	vec3 lightpos = gl_LightSource[0].position.xyz;  // eyespace
	vec4 pos;
	
	
	float rr[8];
	
	float t = 0.0,d = 1.0/float(segs);
	float dt = 1.0/float(segs);
//	float dt = 0.75;
	
	for (i = 0; i < segs; i++) {
	
		vec3 v0,v1;
		float t2 = t*t, d2 = d * d;
		float t3 = t2*t, d3 = d2 * d;
		
			rr[0]=0.2;
			rr[1]=0.2;
			rr[2]=0.2;
			rr[3]=0.2;
			rr[4]=0.2;
			rr[5]=0.2;
			rr[6]=0.2;
			rr[7]=0.2;
		
			
			
		         
		
		// front section
		vec3 pt = (2.*t3 -3.*t2 + 1.) * gl_PositionIn[0].xyz + (-2.*t3 + 3.*t2)* gl_PositionIn[1].xyz
					 + (t3 - 2.*t2 + t) * v0 + (t3 - t2) * v1;
		vec3 ppt = (6.*t2 -6.*t) * gl_PositionIn[0].xyz + (-6.*t2 + 6.*t)* gl_PositionIn[1].xyz
					+ (3.*t2 - 4.*t +1) * v0 + (3.*t2 - 2.*t) * v1;
					 
		spine_f = pt;
		axis_f = normalize (ppt);	 
		span_f = getSpan (axis_f);	
		
		// back section
		pt = (2.*d3 -3.*d2 + 1.) * gl_PositionIn[0].xyz + (-2.*d3 + 3.*d2)* gl_PositionIn[1].xyz
		         + (d3 - 2.*d2 + d) * v0 + (d3 - d2) * v1;
		ppt = (6.*d2 -6.*d) * gl_PositionIn[0].xyz + (-6.*d2 + 6.*d)* gl_PositionIn[1].xyz
		       + (3.*d2 - 4.*d +1) * v0 + (3.*d2 - 2.*d) * v1;
			
		axis_b = normalize (ppt);
		spine_b = pt;
		span_b = getSpan (axis_b);	

		for (j = 0; j <= 12; j++) {                                                     		
			turned_span_f = rodriguesRot(span_f, axis_f, radians(float(j)*360./float(12)));
			pos = vec4 (spine_f + rr[i]*turned_span_f, 1.0);
			vec3 lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));  // eye space
			lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_f), lightvec));
			gl_Position = gl_ModelViewProjectionMatrix * pos;
			myNormal =gl_NormalMatrix * turned_span_f;
			
			if(which == 0)
				gl_TexCoord[0] = vec4 (float(j)/float(12), float(i)/(5.0*float(segs)), 0.0, 1.0);
			if(which == 1)
				gl_TexCoord[0] = vec4 (float(j)/float(12), float(i)/(float(segs))+0.2-float(i)*0.05, 0.0, 1.0);
			EmitVertex();

			
			turned_span_b = rodriguesRot(span_b, axis_b, radians(float(j)*360./float(12)));
			pos = vec4 (spine_b + rr[i+1]*turned_span_b, 1.0);
			lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));
			lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_b), lightvec));
			gl_Position = gl_ModelViewProjectionMatrix * pos;
			
			if(which == 0)
				gl_TexCoord[0] = vec4 (float(j)/float(12), float(i+1)/(5.0*float(segs)), 0.0, 1.0);
			if(which == 1)
				gl_TexCoord[0] = vec4 (float(j)/float(12), float(i+1)/(float(segs))+0.2-float(i+1)*0.05, 0.0, 1.0);
			EmitVertex();
		}

		EndPrimitive();

		t += dt;
		d += dt;
		
	}
	
}