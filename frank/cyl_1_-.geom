#version 120 
#extension GL_EXT_geometry_shader4 : enable

//uniform int segs;
varying out float lightIntensity;  // one per vertex
uniform float radius;
uniform int which;
uniform float vet[15];
vec3 rodriguesRot (vec3 v, vec3 axis, float rad)
{
	return v*cos(rad) + cross(axis,v)*sin(rad) + axis*dot(axis,v)*(1-cos(rad));
}

vec3 getSpan(vec3 axis)
{
	// find a basis with least projection, then gram-schmidt to get perperdicular axis
	int min;
	float minproj = 10;
	vec3 basis[3];
	basis[0] = vec3(1,0,0); basis[1] = vec3(0,1,0); basis[2] = vec3(0,0,1);
	for (int i = 0; i < 3; i++) {
		float m;
		if ((m=abs(dot (axis, basis[i]))) < minproj) {
			minproj = m;
			min = i;
		}
	}
	return radius*normalize(basis[min] - dot(axis,basis[min])*axis);
}

void main(void)
{
	vec3 v0;
	for(int k = 1; k < 8; k++)
		if (which == k)
			v0 = vec3(vet[k-1],vet[k],vet[k+1]);
			
	vec3 turned_span_f,turned_span_b;
	
	vec3 axis_f = normalize (vec3(gl_PositionIn[1]-gl_PositionIn[0]));
	vec3 span_f = getSpan (axis_f);
	vec3 spine_f = gl_PositionIn[0].xyz ;
	
	
	vec3 axis_b = v0;
	vec3 span_b = getSpan (axis_b);
	vec3 spine_b = gl_PositionIn[1].xyz ;
	
	int i, j;

	//Pass-thru!
	for(int i=0; i< gl_VerticesIn; i++){
		gl_Position = gl_ModelViewProjectionMatrix * gl_PositionIn[i];
		EmitVertex();
	}
	EndPrimitive();

	vec3 ecnormal;
	vec3 lightpos = gl_LightSource[0].position.xyz;  // eyespace
	vec4 pos;
	
	
	//vec3 delta_spine = (gl_PositionIn[1].xyz-gl_PositionIn[0].xyz)/float(segs);
	
	

	for (j = 0; j <= 12; j++) {
		turned_span_f = rodriguesRot(span_f, axis_f, radians(float(j)*360./float(12)));
		pos = vec4 (spine_f + turned_span_f, 1.0);
		vec3 lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));  // eye space
		lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_f), lightvec));
		gl_Position = gl_ModelViewProjectionMatrix * pos;
		EmitVertex();
		

        turned_span_b = rodriguesRot(span_b, axis_b, radians(float(j)*360./float(12)));
		pos = vec4 (spine_b + turned_span_b, 1.0);
		lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));
		lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_b), lightvec));
		gl_Position = gl_ModelViewProjectionMatrix * pos;		
		
		//pos = vec4 (spine + delta_spine + turned_span, 1.0);
		lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));
		lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_b), lightvec));
		gl_Position = gl_ModelViewProjectionMatrix * pos;
		EmitVertex();
	}

	EndPrimitive();
	
}