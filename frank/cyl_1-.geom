#version 120 
#extension GL_EXT_geometry_shader4 : enable

//uniform int segs;
varying out float lightIntensity;  // one per vertex
uniform float radius[2];
uniform float vet[6];
uniform int which;
vec3 rodriguesRot (vec3 v, vec3 axis, float rad)
{
	return v*cos(rad) + cross(axis,v)*sin(rad) + axis*dot(axis,v)*(1-cos(rad));
}

vec3 getSpan_f(vec3 axis)
{
	// find a basis with least projection, then gram-schmidt to get perperdicular axis
	int min;
	float minproj = 10;
	vec3 basis[3];
	basis[0] = vec3(1,0,0); basis[1] = vec3(0,1,0); basis[2] = vec3(0,0,1);
	//for (int i = 0; i < 3; i++) {
		//float m;
		//if ((m=abs(dot (axis, basis[i]))) < minproj) {
			//minproj = m;
			//min = i;
		//}
	//}
	min = 0;
	return radius[0]*normalize(basis[min] - dot(axis,basis[min])*axis);
}

vec3 getSpan_b(vec3 axis)
{
	// find a basis with least projection, then gram-schmidt to get perperdicular axis
	int min;
	float minproj = 10;
	vec3 basis[3];
	basis[0] = vec3(1,0,0); basis[1] = vec3(0,1,0); basis[2] = vec3(0,0,1);
	//for (int i = 0; i < 3; i++) {
		//float m;
		//if ((m=abs(dot (axis, basis[i]))) < minproj) {
			//minproj = m;
			//min = i;
		//}
	//}
	min = 0;
	return radius[1]*normalize(basis[min] - dot(axis,basis[min])*axis);
}

void main(void)
{
	float v = 0;
	//vec3 axis = normalize (vec3(gl_PositionIn[1]-gl_PositionIn[0]));
	//vec3 span = getSpan (axis);
	vec3 axis_f,axis_b;
	vec3 span_f,span_b;
	vec3 spine_f,spine_b;
	
	axis_f = normalize(vec3(vet[0],vet[1],vet[2]));axis_b = normalize(vec3(vet[3],vet[4],vet[5]));
	
	span_f = getSpan_f (axis_f);span_b = getSpan_b (axis_b);
	
	spine_f = gl_PositionIn[0].xyz ;spine_b = gl_PositionIn[1].xyz ;
	int i, j;

	//Pass-thru!
	for(int i=0; i< gl_VerticesIn; i++){
		gl_Position = gl_ModelViewProjectionMatrix * gl_PositionIn[i];
		EmitVertex();
	}
	EndPrimitive();

	vec3 ecnormal;
	vec3 lightpos = gl_LightSource[0].position.xyz;  // eyespace
	vec4 pos;
	//vec3 turned_span;
	vec3 turned_span_f,turned_span_b;
	//vec3 delta_spine = (gl_PositionIn[1].xyz-gl_PositionIn[0].xyz)/float(segs);
	//for (i = 0; i < segs; i++) {
		//vec3 spine = gl_PositionIn[0].xyz + float(i)*delta_spine;

		
		for (j = 0; j <= 12; j++) {
			turned_span_f = rodriguesRot(span_f, axis_f, radians(float(j)*360./float(12)));
			pos = vec4 (spine_f + turned_span_f, 1.0);
			vec3 lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));  // eye space
			lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_f), lightvec));
			gl_Position = gl_ModelViewProjectionMatrix * pos;
			
			///材質
			gl_TexCoord[0] = vec4 (float(j)/float(12), float(which)/float(100), 0.0, 1.0);
			EmitVertex();
			
			turned_span_b = rodriguesRot(span_b, axis_b, radians(float(j)*360./float(12)));
			pos = vec4 (spine_b + turned_span_b, 1.0);
			lightvec = normalize (lightpos - vec3(gl_ModelViewMatrix*pos));
			lightIntensity = max (0.0, dot(gl_NormalMatrix*normalize(turned_span_b), lightvec));
			gl_Position = gl_ModelViewProjectionMatrix * pos;
			
			///材質
			gl_TexCoord[0] = vec4 (float(j)/float(12), float(which+1)/float(100), 0.0, 1.0);
			EmitVertex();
		}
        
		EndPrimitive();
	
}