#include <stdlib.h>
#include <gl/glut.h>
#include "svl/svl.h"

Vec3 evalHermite (double t, const Vec3& P1, const Vec3& P2, const Vec3& T1, const Vec3& T2)
{
	// t is in [0,1]
	double t2 = t*t;
	double t3 = t2*t;
	double h1 = 2*t3 - 3*t2 + 1;
	double h2 = -2*t3 + 3*t2;
	double h3 = t3 - 2*t2 +  t;
	double h4 = t3 - t2;
	return Vec3 (P1*h1 + P2*h2 + T1*h3 + T2*h4);	
}

void drawHermite(const Vec3& P1, const Vec3& P2, const Vec3& T1, const Vec3& T2)
{
	glBegin (GL_LINE_STRIP);
	for (double t = 0.0; t <= 1.0; t += 0.1) {
		Vec3 pt = evalHermite (t, P1,P2, T1,T2);
		glVertex2dv (pt.Ref());
	}
	glEnd();
}

