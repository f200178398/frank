#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h> 

#include <SVL/SVL.h>
#include <GL/gluit.h>
#include <GL/glpng.h>
#pragma comment (lib, "svl-dbg.lib")
//#pragma comment (lib, "glew32.lib")

GLuint id,id1;
GLuint pro1,tan_loc,bitan_loc;
// configuration of the chain
#define SEGS 8
Vec3 ctrlpts[SEGS];
double LEN = 0.5;  // length between chain
float me,rl,r1,r2;

double t;

//Vec3 point;
//double angle;
//double speed;

double g = -260;//-260
double f=0.1;

Vec3 anchor1,anchor2,anchor1t(0,2.6,-1.3),anchor2t(0,2.6,-0.8); //   0,2,0 

int pull;

int whichloc=1;
// window ID of the two glut windows
int mainwin, viewer;
Vec3 vet[6];
float varray[6];

vector<double> tk;



///////////////////5/5動工部分
float dis=0,angle=0,x,z,speed=0,angleH=0;
float current_origin_x, current_origin_z;
/////////////////////////////

void display(){}
void reshape(int w, int h)
{
	glViewport(0,0,w,h);
	ViewerReshape(viewer);
}

void grid()           /////劃格線                   
{
	
	glPushAttrib (GL_ENABLE_BIT);
	glDisable (GL_LIGHTING);
	glDisable (GL_TEXTURE_2D);
	glColor3f (1,1,1);
	glLineWidth (1.0);
	glBegin (GL_LINES);
	for (int i = -10; i <= 10; i++) {
		glVertex3i (-10,0,i); glVertex3i (10,0,i);
		glVertex3i (i,0,-10); glVertex3i (i,0,10);
	}
	glEnd();
	glLineWidth (1.0);
	glBegin (GL_LINES);
	glColor3f (1,0,0), glVertex3i (0,0,0); glVertex3i (5,0,0);
	glColor3f (0,1,0), glVertex3i (0,0,0); glVertex3i (0,5,0);
	glColor3f (0,0,1), glVertex3i (0,0,0); glVertex3i (0,0,5);
	glEnd();
	glPopAttrib();
	
}          /////劃格線   
static Vec3 pold[SEGS];

void init1()            
{
	for (int i = 0; i < SEGS; i++) 
		pold[i] = ctrlpts[i] = Vec3 (LEN*i, 7,0);
	ctrlpts[1] = anchor1;
}

enum para_type {UNIFORM, CHORDAL, CENTRIPETAL};
para_type para;

void uniform_para()
{
	tk.clear();
	tk.push_back (0.0);
	for (int i = 1; i < 10; i++) 
		tk.push_back (1 + tk[i-1]);
}

void constraint()             /////////////固定兩點距離
{
	double rest = LEN;
	
	for (int i = 0; i < SEGS-1; i++) {
		Vec3 delta = ctrlpts[i+1] - ctrlpts[i];
		double deltalen = len (delta);
		double diff = (deltalen - rest)/deltalen;

		ctrlpts[i] += delta*0.5*diff;
		ctrlpts[i+1] -= delta*0.5*diff;

	}
}


Vec3 center(0,5,0);
void collision ()                 ///////碰撞偵測  &反應
{
	
	
	for (int i = 0; i < SEGS; i++) {
		if (len (ctrlpts[i]- center) < 1) {
			ctrlpts[i] = norm (ctrlpts[i]-center)+center;
		}
	}


}
// sometimes it got stuck below floor, not able to bounce back
// consider do a p1 correction (seems to solve the problem)

void update(double dt)
{
	// here: static store pold
	// compute pnew --> p(t+dt) with p & pold
	// p --> pold
	// pnew --> p
	
	Vec3 pnew[SEGS];
	
	Vec3 mousespring;
	if (pull) {
		mousespring = 23*( ctrlpts[SEGS-1]);
	} else {
		mousespring = vl_0;
	}

	for (int i = 0; i < SEGS; i++) {
		Vec3 force = Vec3(0,g,0);
		if (i == SEGS-1) force += mousespring;
		pnew[i] = (2-f)*ctrlpts[i] - (1-f)*pold[i] + force*dt*dt;

		pold[i] = ctrlpts[i];
		ctrlpts[i] = pnew[i];
	}
	
	// no wall collision in this case
	collision();

	constraint();
	// anchor p1...
	ctrlpts[1] = anchor2;
	//ctrlpts[2] = anchor1;
	
	
}



void chain(){        ///////畫鍊條(頭髮)
	glPointSize (20);
	//cout << p[0] << endl;
	//glBegin (GL_POINTS);
	//	for (int i = 1; i < SEGS-1; i++)
	//		glVertex3dv (ctrlpts[i].Ref());
			
	//glEnd();


	
	para = UNIFORM;
	uniform_para();
	glColor3f (.85,0,0);
	extern void drawCatmull_barry();
	drawCatmull_barry();
	/*glLineWidth (1.0);
	glBegin (GL_LINE_STRIP);
		for (int i = 0; i < SEGS; i++) 
			glVertex3dv (ctrlpts[i].Ref());
	glEnd();*/
}

/////////////5/5動工部分
GLMmodel *m1;

void drawcar()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glPushMatrix();
	glTranslatef(-.02,0,-0.05);
	glmDraw (m1, GLM_SMOOTH|GLM_MATERIAL);
	glPopMatrix();
	glPopAttrib();
	glEnd();
}

////////////////////////

int vloc,addloc;
float radiusloc,rr[2],add[1];
void content()
{
	
	glClear (GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(0);
	grid();

	/////////////////////////////////5/5動工
	x = current_origin_x + dis*sin(angle*3.14/180);
    z = current_origin_z + dis*cos(angle*3.14/180);

	glPushMatrix();
	glTranslatef (x,0,z+0.0);
	glRotatef (angle, 0,1,0);
	
	
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glPushMatrix();
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	drawcar();
	glPopMatrix();
	glPopAttrib();
	
	
	//angleH = angle;
	anchor2 = Vec3(x*10,5,z*10);
	//anchor1 =Vec3(x*10,2.3,.7+z*10);
	
	//angleH = 0;
	////////////////////////////////////////
	glPopMatrix();

	extern vector<Vec3> myarray,myvec;
	
    glPushMatrix();
	glScalef(.1,.1,.1);
	//glTranslatef(0,3,0);
	glUseProgram(0);
	chain(); //原來的鍊子
	
	
	glUseProgram(0);
	glBegin (GL_POINTS);         //證明我找的點與鍊子是同樣的
	for(int o=0; o<=myvec.size() ;o++)
		glVertex3dv(myarray[o].Ref());
	glEnd();
	
	
	glUseProgram(pro1);    //要畫圓柱部分

	/////////////////////////////////////////////
	glUniform1i (glGetUniformLocation (pro1,"NormalMap"), 0);
	glUniform1i (glGetUniformLocation (pro1,"ColorMap"), 1);
	glActiveTexture (GL_TEXTURE0);
	glBindTexture (GL_TEXTURE_2D, id);
	glActiveTexture (GL_TEXTURE1);
	glBindTexture (GL_TEXTURE_2D, id1);
		
	tan_loc = glGetAttribLocation (pro1,"Tangent");
	bitan_loc = glGetAttribLocation (pro1,"Bitangent");
	/////////////////////////////////////////////
	
	double p=0.5,p_b=0.49;
	for(int o=0; o<myvec.size()-1 ;o++)
	{
		glUniform1i (whichloc,o);
		varray[0] = myvec[o][0];   varray[1] = myvec[o][1];   varray[2] = myvec[o][2];
		varray[3] = myvec[o+1][0]; varray[4] = myvec[o+1][1]; varray[5] = myvec[o+1][2];
		rr[0]=rr[1]=0.2;
		if(o >= 15)
		{
			rr[0]=0.2+(o-15)/2*0.01;
			rr[1]=0.2+(o+1-15)/2*0.01;
		}

		if(0.2+(o-15)/2*0.01 >= 0.5)
		{
			/*rr[0]=p;
			rr[1]=p_b;
			
			p-=0.01;
			p_b=p-0.01;*/
			rr[0]=0.5;
			rr[1]=0.5;
		}
		
		glBindTexture(GL_TEXTURE_2D,id);
		glUniform1fv (vloc, 6, varray);
		glUniform1fv (radiusloc,2,rr);
		glBegin (GL_LINES);
		glVertex3dv(myarray[o].Ref());
		glVertex3dv(myarray[o+1].Ref());
		glEnd();
	}
	
	
	/*glUseProgram(0);
	glEnable (GL_POINT_SMOOTH);
	
	

	// FOR "YES"
	glPushMatrix();
		glTranslatef(0,1,0);
		
	
		glRotatef(angle,1,0,0);
		glTranslatef(0,1,0);
		

		glColor3f (1,0,0);
		glBegin (GL_POINTS);
		glVertex3i (0,0.5,1);
		glEnd();
		glColor3f (1,1,1);
		
		glColor3f (0,0,1);
		
		glLineWidth (1.0);	
		glColor3f (1,1,1);
		//glutWireSphere( 1.0, 20.0, 20.0); 
		glColor3f (1,0,0);

		
		
	glPopMatrix();

	center = Vec3(0,cos(angle/180.*3.14)+1,sin(angle/180.*3.14));

	glBegin (GL_POINTS);
	glVertex3f(center[0],center[1],center[2]);
		glEnd();*/
   //center = Vec3(0,2,0);
   glPopMatrix();
   glPopAttrib();
   

	glutSwapBuffers();
}

void special (int key, int x, int y)
{
#define CLAMP(v,lo,hi)  ((v) < (lo) ? (lo) : ((v) > (hi) ? (hi) : (v)))
	switch (key) {
		
		case GLUT_KEY_RIGHT:
			if(angle>0){
				angle -= 2;
				//anchor1 = Vec3(0,cos(angle/180.*3.14)+1.7,sin(angle/180.*3.14)-1);
				//anchor2 = center + Vec3(0,1,0);
				//printf("%f\n",-cos(angle/180.*3.14));
				anchor2 -= Vec3(0,0.01,0.05);
				anchor1 -= Vec3(0,0.01,0.05);
			}
		break;
		case GLUT_KEY_LEFT:
			if(angle<16){
				angle += 2;
				//anchor1 = Vec3(0,cos(angle/180.*3.14)+1.7,sin(angle/180.*3.14)-1);
				//anchor2 = center + Vec3(0,1,0);
				//anchor2 = Vec3(0,2+2*sin(angle/180.*3.14),-2*cos(angle/180.*3.14));
				//printf("%f\n",-cos(angle/180.*3.14));
				anchor2 += Vec3(0,0.01,0.05);
				anchor1 += Vec3(0,0.01,0.05);
				}
		break;
	}
	glutPostRedisplay();
}


void timer (int dummy)
{
	glutTimerFunc (50, timer, 0);

	t += 0.01;
	update(0.01);
	dis += speed;
	glutPostRedisplay();
}

void init()
{
	
	glEnable (GL_DEPTH_TEST);
	glLineWidth (1.0);
	glClearColor (.4,.4,.4,1);

	///////////////////////////////////////////
	// reading OBJ model
	m1 = glmReadOBJ ("SA4.obj");
	glmUnitize (m1);
	glmFacetNormals (m1);
	glmVertexNormals (m1, 90);
	///////////////////////////////////////////


	//glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	extern GLuint setVFGShaders(char*,char*,char*);
	pro1 = setVFGShaders ("cyl_1-.vert", "cyl_1-.frag", "cyl_1-.geom"); 
	
	////////////////////////////////
	glProgramParameteriEXT(pro1,GL_GEOMETRY_INPUT_TYPE_EXT,GL_LINES);
	glProgramParameteriEXT(pro1,GL_GEOMETRY_OUTPUT_TYPE_EXT,GL_TRIANGLE_STRIP);

	
	
	int temp,i=0.1;
	glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT,&temp);
    glProgramParameteriEXT(pro1,GL_GEOMETRY_VERTICES_OUT_EXT,temp);

	////////////////////////

	glLinkProgram(pro1);
	glUseProgram(pro1);
	
	int radiusloc = glGetUniformLocation (pro1, "radius");
	//glUniform1f (radiusloc, 0.5);
	whichloc = glGetUniformLocation (pro1, "which");
	//int segloc = glGetUniformLocation (pro1, "segs");
    //glUniform1i (segloc, 7);
	vloc = glGetUniformLocation (pro1, "vet");
	//addloc = glGetUniformLocation (pro1, "add");
	

	float lightpos[] = {0,0,2,1};
	glLightfv (GL_LIGHT0, GL_POSITION, lightpos);

	pngSetStandardOrientation (1);
	id = pngBind("12311.png", PNG_NOMIPMAP, PNG_SOLID, NULL, GL_CLAMP, GL_NEAREST, GL_NEAREST);
	id1 = pngBind("1t2.png", PNG_NOMIPMAP, PNG_SOLID, NULL, GL_CLAMP, GL_NEAREST, GL_NEAREST);
	//pngSetStandardOrientation(1);
}

void key(unsigned char k, int x1, int y1)
{
	   switch (k){ 
	   case 'w':
			speed+=0.05;
		    break;
	   case 'a':
			current_origin_x = x; current_origin_z = z;
			dis=0.0;
			angle+=2;
		    break;
	   case 'd':
			
			current_origin_x = x; current_origin_z = z;
			dis=0.0;
			angle-= 2;
		    break;
	   }
	

	glutPostRedisplay();
}


void main (int argc, char** argv)
{
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGB|GLUT_DOUBLE|GLUT_DEPTH);
	glutInitWindowSize (400,400);
	mainwin = glutCreateWindow ("drive [ARROW]");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);

	viewer = ViewerNew (mainwin, 0,0,400,400, content);
	
	glewInit();
	if (glewIsSupported("GL_VERSION_2_1"))
		printf("Ready for OpenGL 2.1\n");
	else {
		printf("OpenGL 2.1 not supported\n");
		exit(1);
	}
	if (GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader && GL_EXT_geometry_shader4)
		printf("Ready for GLSL - vertex, fragment, and geometry units\n");
	else {
		printf("Not totally ready :( \n");
		exit(1);
	}

	
	init();
	init1();
	glutKeyboardFunc(key);
	glutSpecialFunc (special);
	glutTimerFunc (500, timer, 0);

	glutMainLoop();
}
