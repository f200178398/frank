#include <stdlib.h>
#include <gl/glut.h>
#include "svl/svl.h"
#include <vector>
using namespace std;
extern Vec3 center;
vector<Vec3> myarray,myvec;
Vec3 evalCatmull (double t, const Vec3& P0,const Vec3& P1,const Vec3& P2,const Vec3& P3,
		double t0, double t1, double t2, double t3)
{
	Vec3 A1 = (P0*(t1-t) + P1*(t-t0))/(t1-t0);
	Vec3 A2 = (P1*(t2-t) + P2*(t-t1))/(t2-t1);
	Vec3 A3 = (P2*(t3-t) + P3*(t-t2))/(t3-t2);

	Vec3 B1 = (A1*(t2-t) + A2*(t-t0))/(t2-t0);
	Vec3 B2 = (A2*(t3-t) + A3*(t-t1))/(t3-t1);
	
	return (B1*(t2-t) + B2*(t-t1))/(t2-t1);
}
 
void drawCatmull_barry ( )
{
	myarray.clear();
	myvec.clear();
	extern Vec3 ctrlpts[8];
	extern vector<double> tk;
	
	Vec3 foot;
	//myarray.push_back(center);
	glBegin (GL_LINE_STRIP);
	double ds = (tk[6] - tk[1])/100;
	for (double s = tk[1]; s <= tk[6]; s+= ds) {
		for (int i = 6; i >= 1; i--) {
			if (s >= tk[i]) {
				foot = evalCatmull (s, ctrlpts[i-1], ctrlpts[i], ctrlpts[i+1], ctrlpts[i+2], tk[i-1],tk[i],tk[i+1],tk[i+2]);
				
				break;
			}
		}
		myarray.push_back(foot);
		glVertex3dv (foot.Ref());
		/*glBegin(GL_POINTS);
		glVertex3dv (foot.Ref());
		glEnd();*/
	}	
	glEnd();

	for(int o=0; o<myarray.size()-3; o++)
			myvec.push_back((myarray[o+1]-myarray[o])/ds);
	//myvec.push_back(myarray[myvec.size()]);
	//myvec.push_back(((myarray[myarray.size()-2]-myarray[myarray.size()-3])/ds));
	//printf("%d   %d\n",myarray.size(),myvec.size());

}
