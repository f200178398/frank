#include <stdlib.h>
#include "gl/gluit.h"
#include "svl/svl.h"
#include <vector>
using namespace std;

#pragma comment (lib, "gluit-vc9.lib")
#pragma comment (lib, "svl-vc9.lib")

//vector<Vec2> ctrlpts;
Vec2 ctrlpts[10];
vector<double> tk;

int done=1;
int gw=400;
int gh=400;

void reshape (int w, int h)
{
	gw = w; gh = h;
	glViewport (0,0,gw,gh);
	glMatrixMode (GL_PROJECTION); glLoadIdentity(); gluOrtho2D (-10,10,-10,10);
	glMatrixMode (GL_MODELVIEW); glLoadIdentity();
}

void drawCatmull()
{
	extern void drawCatmull_barry (const Vec2&,const Vec2&,const Vec2&,const Vec2&);
	for (int i = 1; i < 8; i++) {
		drawCatmull_barry (ctrlpts[i], ctrlpts[i+1], (ctrlpts[i+1]-ctrlpts[i-1])/(tk[i+1]-tk[i-1]), (ctrlpts[i+2]-ctrlpts[i])/(tk[i+2]-tk[i]));
	}
}
void initDate()
{
	ctrlpts[0] = Vec2(0,-1);
	ctrlpts[1] = Vec2(1,0);
	ctrlpts[2] = Vec2(2,1);
	ctrlpts[3] = Vec2(3,0);
	ctrlpts[4] = Vec2(4,-1);
	ctrlpts[5] = Vec2(5,0);
	ctrlpts[6] = Vec2(6,1);
	ctrlpts[7] = Vec2(7,0);
	ctrlpts[8] = Vec2(8,-1);
	ctrlpts[9] = Vec2(9,0);
}

Vec2 foot;
int in_move;

enum para_type {UNIFORM, CHORDAL, CENTRIPETAL};
para_type para;

void uniform_para()
{
	tk.clear();
	tk.push_back (0.0);
	for (int i = 1; i < 10; i++) 
		tk.push_back (1 + tk[i-1]);
}

void display ()
{
	glClear (GL_COLOR_BUFFER_BIT);
	
	int i;
	glColor3f (0,0,0);
	glPointSize (10.0);
	glBegin (GL_POINTS);
	for (i = 0; i < 10; i++) 
		glVertex2dv (ctrlpts[i].Ref());
	glEnd();

	para = UNIFORM;
	uniform_para();
	if(done){
		glColor3f (.85,0,0);
		extern void drawCatmull_barry();
		drawCatmull_barry();

//		drawCatmull();
	}

	
	
	BEGIN_2D_OVERLAY(10,10);
	if (para == UNIFORM) drawstr (1,1,"uniform");
	if (para == CHORDAL) drawstr (1,1,"chordal");
	if (para == CENTRIPETAL) drawstr (1,1,"centripetal");
	END_2D_OVERLAY();

	glutSwapBuffers();
}

void init()
{
	glEnable (GL_POINT_SMOOTH);
	glClearColor (1,1,1,1);
	glLineWidth (5.0);
}

void main (int argc, char** argv)
{
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGB|GLUT_DOUBLE);
	glutInitWindowSize (500,500);
	glutInitWindowPosition (200,200);
	glutCreateWindow ("[cr] [udt] [m]");
	
	glutReshapeFunc (reshape);
	glutDisplayFunc (display);

	initDate();
	init();
	
	glutMainLoop();
}
