//CIRL GPU Fragment Program: Derek Anderson and Robert Luke
// very simple fragment shader
//varying vec3 myNormal;
//varying float lightIntensity;
uniform sampler2D tex;
//varying vec3 lightDir;
//uniform sampler2D NormalMap;
//uniform sampler2D ColorMap;
void main()
{	
#if 0
	float ndotl = max(0., dot(normalize(ecnormal_out), vec3(0,0,1)));
	ndotl = 0.4;
	gl_FragColor = vec4(ndotl * vec3(0.0,0.0,1.0),1.0);	
#else
	//gl_FragColor = vec4 (lightIntensity*vec3(1,1,1), 1.0);
	gl_FragColor = texture2D (tex, gl_TexCoord[0].st);//←only 原
	
	//vec3 norm = vec3 (texture2D (NormalMap, gl_TexCoord[0].st));
	//norm = (norm - 0.5)*2.0;
	//float intensity1 = dot (lightDir, norm);

	//vec4 color = intensity1*texture2D (ColorMap, gl_TexCoord[0].st);
	//gl_FragColor = color;
#endif
}
